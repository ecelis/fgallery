FROM debian
RUN apt-get update \
    && apt-get -y install imagemagick exiftran zip liblcms2-utils libimage-exiftool-perl \
    libjson-perl libjson-xs-perl jpegoptim pngcrush p7zip python-opencv libopencv-dev unp unzip fish wget python-numpy

# RUN wget https://gitlab.com/korzec/fgallery/-/archive/master/fgallery-master.zip \
#     && unzip fgallery-master.zip \
#     && move fgallery-master fgallery
COPY . /fgallery
ENV PATH "$PATH:/fgallery"

VOLUME [ "/data" ]

WORKDIR /data

ENTRYPOINT ["fgallery"]

# example use 
# docker build -t fgallery .
# docker run -v D:\\data:/data --rm fgallery t1 t1x
